package com.example.firstapp.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.firstapp.MessageViewModel
import com.example.firstapp.adapters.MessageAdapter
import com.example.firstapp.databinding.FragmentChatBinding
import com.example.firstapp.models.MessageResponseItem
import com.example.firstapp.models.RequestBody
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class Chat : Fragment() {

    private lateinit var binding: FragmentChatBinding
    private lateinit var messageAdapter: MessageAdapter
    private val viewModel: MessageViewModel by viewModels()
    private lateinit var phone: String
    private var messages: ArrayList<MessageResponseItem> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentChatBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        phone = ChatArgs.fromBundle(requireArguments()).phone


//        viewModel.getMessages(phone.toLong())
        messageAdapter = MessageAdapter()
        binding.recyclerView.adapter = messageAdapter


        observeData()

        binding.sendButton.setOnClickListener {
            val msg  = binding.messageText.text.toString().trim()
            if (msg.isEmpty()) {
                binding.messageText.requestFocus()
                binding.messageText.error = "Message is required"
            } else {

                val body: RequestBody = RequestBody(
                    msg,
                    phone.toLong()
                )
                messages.add(MessageResponseItem("Now", 1234L, 0, phone.toLong(), msg, 0))
                messageAdapter.setItems(messages)
                messageAdapter.notifyDataSetChanged()
                binding.recyclerView.scrollToPosition(messageAdapter.itemCount -1)
                viewModel.sendMessage2(body).observe(viewLifecycleOwner, Observer {
                    if (it != null) {
                       observeData()
                    }

                })
                binding.messageText.setText("")
            }
        }

    }

    private fun observeData() {
        viewModel.getMessages2(phone.toLong()).observe(viewLifecycleOwner, Observer {
            Log.d("OBSERVER", "I HAVE SOME DATA RIGHT HERE")
            messages.clear()
            messages.addAll(it)

            messageAdapter.setItems(messages)
            messageAdapter.notifyDataSetChanged()
            binding.recyclerView.scrollToPosition(messageAdapter.itemCount -1)
        })
    }


}