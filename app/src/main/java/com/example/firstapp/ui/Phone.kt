package com.example.firstapp.ui

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.firstapp.databinding.FragmentPhoneBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class Phone : Fragment() {

    private lateinit var binding: FragmentPhoneBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPhoneBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.enterButton.setOnClickListener {
            val phone = binding.userPhone.text.toString().trim()

            if (TextUtils.isEmpty(phone)) {
                binding.userPhone.requestFocus()
                binding.userPhone.error = "Enter valid phone"
            } else {
                val action = PhoneDirections.actionPhoneToChat(phone)
                findNavController().navigate(action)
            }

        }
    }
}