package com.example.firstapp.data

import com.example.firstapp.R
import com.example.firstapp.models.DisplayMessage

class DataSource {
    fun loadMessages() : List<DisplayMessage> {

        return listOf<DisplayMessage>(
            DisplayMessage(R.string.message1),
            DisplayMessage(R.string.message2),
            DisplayMessage(R.string.message3),
            DisplayMessage(R.string.message4),
            DisplayMessage(R.string.message5),
            DisplayMessage(R.string.message6),
            DisplayMessage(R.string.message7),
            DisplayMessage(R.string.message8),
            DisplayMessage(R.string.message9),
            DisplayMessage(R.string.message10)
        )
    }
}