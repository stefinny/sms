package com.example.firstapp.models


import com.google.gson.annotations.SerializedName

data class MessageReply(
    val message: String,
    val reply: List<MessageResponseItem>,
    val status: Int
)