package com.example.firstapp.models


import com.google.gson.annotations.SerializedName

data class MessageResponseItem(
    @SerializedName("date_created")
    val date_created: String,
    val destination: Long,
    val id: Int,
    val source: Long,
    val text: String,
    val type: Int
)