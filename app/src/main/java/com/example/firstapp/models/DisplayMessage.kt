package com.example.firstapp.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

data class DisplayMessage(val resourceId: Int) {
}


