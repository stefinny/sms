package com.example.firstapp.models

data class RequestBody(
    val text: String,
    val source: Long
) {

}