package com.example.firstapp.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity()
data class Message(

    @PrimaryKey(autoGenerate=true)
    var id: Long = 0L,

    @ColumnInfo(name = "text")val text: String?,
    @ColumnInfo(name = "type")val type: Int?,
    @ColumnInfo(name = "source")val source: Long?,
    @ColumnInfo(name = "destination")val destination: Long?,
    @ColumnInfo(name = "date_created")val date_created: Long?
)