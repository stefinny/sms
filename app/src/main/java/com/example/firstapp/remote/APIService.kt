package com.example.firstapp.remote

import com.example.firstapp.models.MessageReply
import com.example.firstapp.models.MessageResponse
import com.example.firstapp.models.MessageResponseItem
import com.example.firstapp.models.RequestBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface APIService {

    @GET("messages/{phone}/")
    suspend fun getMessages(@Path("phone") phone: Long):
            Response<ArrayList<MessageResponseItem>>


    @POST("messages/")
    suspend fun sendMessage(@Body requestBody: RequestBody): Response<MessageReply>
}