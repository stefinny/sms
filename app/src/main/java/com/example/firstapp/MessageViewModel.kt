package com.example.firstapp

import android.util.Log

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.example.firstapp.models.MessageResponseItem
import com.example.firstapp.models.RequestBody
import com.example.firstapp.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class MessageViewModel @Inject constructor(

    private val repository: Repository
    ): ViewModel() {

    private var mResponses = ArrayList<MessageResponseItem>()
    private val _resp = MutableLiveData<List<MessageResponseItem>>()


//    val getMsg: MutableLiveData<List<MessageResponseItem>>
//        get() = _resp

    fun getMessages2(phone:Long) = liveData<ArrayList<MessageResponseItem>>{
       val resp = repository.getMessages(phone)
        if (resp.isSuccessful) {
            mResponses = resp.body()!!
            emit(mResponses)
        }
    }

//    fun getMessages(phone: Long) = viewModelScope.launch {
//
//        repository.getMessages(phone).let {
//            if (it.isSuccessful) {
//                mResponses = it.body()!!
//                _resp.value = mResponses
//            }
//        }
//    }

//    fun sendMessage(requestBody: RequestBody) = viewModelScope.launch {
//
//        // saving current message so that it's displayed by recyclerview before sending to server
//        mResponses.add(MessageResponseItem("", 1234L, 1, requestBody.source, requestBody.text, 0))
////        _resp.value = mResponses
//
//
//        repository.sendMessage(requestBody).let {
//            if (it.isSuccessful) {
//                val iterator = it.body()?.reply?.iterator()
//                if (iterator != null) {
//                    while (iterator.hasNext()) {
//                        mResponses.add(iterator.next())
//                    }
//                    _resp.value = mResponses
//                }
//
//            } else {
//                Log.d("POSTMETHOD FAILURE", it.body().toString())
//
//            }
//        }
//    }

    fun sendMessage2(requestBody: RequestBody) = liveData{
        val resp = repository.sendMessage(requestBody)
        if (resp.isSuccessful) {

            emit(resp.body())
        }else{
            emit(resp.errorBody())
        }
    }
}