package com.example.firstapp.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.firstapp.R
import com.example.firstapp.models.MessageResponse
import com.example.firstapp.models.MessageResponseItem
import java.lang.IllegalArgumentException


class MessageAdapter()
    : RecyclerView.Adapter<MessageAdapter.MessageHolder>() {

    private var dataset: List<MessageResponseItem> = ArrayList()
    fun setItems(dataset: List<MessageResponseItem>){
        this.dataset = dataset
    }


    inner class MessageHolder(view: View) : RecyclerView.ViewHolder(view) {
        val shortcode: TextView
        val message_body: TextView
        val mesage_time: TextView

        init {
            Log.d("MESSAGE_HOLDER", "MESSAGEHOLDER INITIALIZER")
            shortcode = view.findViewById(R.id.shortcode)
            message_body = view.findViewById(R.id.received_message_body)
            mesage_time = view.findViewById(R.id.received_message_time)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.received_message, parent, false)
        return MessageHolder(view)
    }

    override fun onBindViewHolder(holder: MessageHolder, position: Int) {

        holder.shortcode.text = dataset[position].source.toString()
        holder.message_body.text = dataset[position].text
        holder.mesage_time.text = dataset[position].date_created

    }

    override fun getItemCount(): Int = dataset.size

}