package com.example.firstapp.repository

import androidx.lifecycle.LiveData
import com.example.firstapp.models.MessageResponse
import com.example.firstapp.models.RequestBody
import com.example.firstapp.remote.APIService
import javax.inject.Inject


class Repository @Inject constructor(
    private val apiService: APIService
) {
    suspend fun getMessages(phone: Long) = apiService.getMessages(phone)

    suspend fun sendMessage(requestBody: RequestBody) = apiService.sendMessage(requestBody)
}